﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileMoveSlime : MonoBehaviour
{
    public Vector3 point1, point2, point3;
    private Transform _transform;
    private GameObject player;

    private float count = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        point1 = gameObject.transform.position;
        player = GameObject.FindGameObjectWithTag("Player");
        point3 = player.transform.position;
        point2 = point1 + (point3 - point1) / 2 + Vector3.up * 2.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (count < 1.0f)
        {
            count += 1.0f * Time.deltaTime;
            Vector3 m1 = Vector3.Lerp( point1, point2, count );
            Vector3 m2 = Vector3.Lerp( point2, point3, count );
            gameObject.transform.position = Vector3.Lerp(m1, m2, count);
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
    }
}

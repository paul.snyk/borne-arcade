﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlime : MonoBehaviour
{
    public float speed, distance, distanceAttack;
    private bool movingRight = true;
    public GameObject bulletSlime;
    public LayerMask layers;

    public float timeNextFire;
    public bool nextFire = false;
    private Transform _transformSlime;
    public Transform groundDetection;

    private void Start()
    {
        _transformSlime = GetComponent<Transform>();//on recup transform du slime au lancement
        
    }

    void Update() //un raycast regarde continuellement en bas pour voir s'il y a encore un sol, s'il n'y en a pas, il se retourne
    {
        
        transform.Translate(Vector2.right * speed * Time.deltaTime);//le slime effectue un translate
        RaycastHit2D groundInfo = Physics2D.Raycast(groundDetection.position, Vector2.down, distance,layers);//c'est un raycast sur le transform du ground check
        if (groundInfo.collider == false)//on regarde si il y a un sol en dessous du slime
        {
            if (movingRight == true)
            {
                transform.eulerAngles = new Vector3(0, -180,0 );//on rotate le sprite pour le voir partir vers la droite
                movingRight = false;// on passe le moving right en false pour préparé le passage des move vers la gauche
            }
            else
            {
                transform.eulerAngles = new Vector3(0, 0,0 );//on rotate le sprite pour le voir partir vers la gauche
                movingRight = true;// on passe le moving right en true pour préparé le passage des move vers la droite
            }
        }

        if (movingRight == true)
        {
            RaycastHit2D checkPlayerRight = Physics2D.Raycast(_transformSlime.position, Vector2.right, distanceAttack);//on créer raycast vers la gauche
            Debug.DrawRay(_transformSlime.position, Vector2.right * distanceAttack,Color.red);// c'est un debug draw ray pour voir le rayon de detection'
            if (checkPlayerRight.collider == null)
            {
                timeNextFire = 0f; 
            }
            else if (checkPlayerRight.collider.CompareTag("Player"))//on constater une collision ducoup on check le tag de l'object avec lequel on a eu la collision et ducoup si c'est un object avec tag player alors
            {
                if (nextFire == false)//tous les if vont permettre de crée une cadence de tiré pour le slime et instantiate le missile
                {
                    FireEnnemy();
                    nextFire = true;
                }
                if (nextFire == true)
                {
                    timeNextFire += Time.deltaTime;
                }
                if (timeNextFire >= 1.2f)
                {
                    nextFire = false;
                    timeNextFire = 0f;
                }
            }
        }
        if (movingRight == false)
        {
            RaycastHit2D checkPlayerLeft = Physics2D.Raycast(_transformSlime.position, Vector2.left, distanceAttack);//on créer raycast vers la gauche
            Debug.DrawRay(_transformSlime.position, Vector2.left * distanceAttack,Color.blue);// c'est un debug draw ray pour voir le rayon de detection'
            if (checkPlayerLeft.collider == null)
            {
                timeNextFire = 0f; 
            }
            else if (checkPlayerLeft.collider.CompareTag("Player"))//on constater une collision ducoup on check le tag de l'object avec lequel on a eu la collision et ducoup si c'est un object avec tag player alors
            {
                if (nextFire == false)//tous les if vont permettre de crée une cadence de tiré pour le slime et instantiate le missile
                {
                    
                    FireEnnemy();
                    nextFire = true;
                }
                if (nextFire == true)
                {
                    timeNextFire += Time.deltaTime;
                }

                if (timeNextFire >= 1.2f)
                {
                    nextFire = false;
                    timeNextFire = 0f;
                }
                
            }
        }
    }

    void FireEnnemy()
    {
        Instantiate(bulletSlime, _transformSlime.position, _transformSlime.rotation);
    }
    
}

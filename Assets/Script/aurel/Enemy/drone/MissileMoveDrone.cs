﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileMoveDrone : MonoBehaviour
{
    private Vector3 playerPosition;
    private Transform _transform;
    private GameObject player;
    private float speed = 5f;
    void Start()
    {
        _transform = GetComponent<Transform>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerPosition = player.transform.position;
        transform.LookAt(playerPosition);
    }

    private void Update()
    {
        transform.Translate(Vector3.forward*speed*Time.deltaTime);
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Ground"))
        {
            Destroy(gameObject);
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyDrone : MonoBehaviour
{
    public Vector2 spherecast;
    private Transform _transform;
    public float timeNextFire;
    public bool nextFire = false;
    public GameObject bulletDrone;
    public GameObject player;
    public GameObject pointBullet;

    public float stoppingDistance, nearDistance;
    public float speed;
    public bool stopMovement = false;
    //private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        _transform = GetComponent<Transform>();
        //_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
        
         RaycastHit2D[] raycastDetection = Physics2D.CircleCastAll(_transform.position, 8f, transform.position);
         foreach (var hit in raycastDetection)
         {
             if (hit.collider.CompareTag("Player"))// si joueur rentre dans les limite du circle caste alors
             {
                 if (Vector2.Distance(transform.position, player.transform.position) < nearDistance)
                 {
                     transform.position = Vector2.MoveTowards(transform.position, player.transform.position, -speed * Time.deltaTime);
                 } 
                 else if (Vector2.Distance(transform.position, player.transform.position) > stoppingDistance)
                 {
                     transform.position = Vector2.MoveTowards(transform.position, player.transform.position, speed * Time.deltaTime);
                 }
                 else if (Vector2.Distance(transform.position, player.transform.position) < stoppingDistance && Vector2.Distance(transform.position, player.transform.position) > nearDistance)
                 {
                     gameObject.transform.position = this.transform.position;
                 }

                 if (transform.position.y < player.transform.position.y)
                 {
                     transform.Translate(Vector3.up * Time.deltaTime);
                 }
                 if (transform.position.y > player.transform.position.y)
                 {
                     transform.Translate(Vector2.down * Time.deltaTime);
                 }
                 if (transform.position.x > player.transform.position.x) // Compare la position en x du player et de l'ennemi, il se tournera en fonction de la position du joueur
                 {
                     transform.localScale  = new Vector3(-0.1f,0.1f,0.1f);
                 } 
                 else 
                 {
                     transform.localScale  = new Vector3(0.1f,0.1f,0.1f);
                 }
             }
         }
        
        
        RaycastHit2D[] raycastAttack = Physics2D.CircleCastAll(_transform.position, 3f, transform.position);
        foreach (var hit in raycastAttack)
        {
            if (hit.collider.CompareTag("Player"))// si joueur rentre dans les limite du circle caste alors
            {
                if (nextFire == false)//tous les if vont permettre de crée une cadence de tiré pour le slime et instantiate le missile
                {
                    FireEnemy();
                    nextFire = true;
                }
                if (nextFire == true)
                {
                    timeNextFire += Time.deltaTime;
                }
                if (timeNextFire >= 2.3f)
                {
                    nextFire = false;
                    timeNextFire = 0f;
                }
            }
        }
    }

    void FireEnemy()
    {
        Instantiate(bulletDrone, pointBullet.transform.position, pointBullet.transform.rotation);
    }
}

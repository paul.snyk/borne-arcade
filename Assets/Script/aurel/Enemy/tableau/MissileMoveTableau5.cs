﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MissileMoveTableau5 : MonoBehaviour
{
    public Vector3 point1, point2, point3;
    private Transform _transform;
    public GameObject finishPoint;
    private float count = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        finishPoint = GameObject.Find("Zone 5");
        point1 = gameObject.transform.position;
        point3 = finishPoint.transform.position;
        point2 = point1 + (point3 - point1) / 2 + Vector3.up * 4.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (count < 1.0f)
        {
            count += 1.0f * Time.deltaTime;
            Vector3 m1 = Vector3.Lerp( point1, point2, count );
            Vector3 m2 = Vector3.Lerp( point2, point3, count );
            gameObject.transform.position = Vector3.Lerp(m1, m2, count);
        }

        if (gameObject.transform.position == point3)//lorsque position gameobject = position point3 alors destroy après l'animation
        {
            //mettre l'anim avant destruction
            Destroy(gameObject);
        }
    }
}

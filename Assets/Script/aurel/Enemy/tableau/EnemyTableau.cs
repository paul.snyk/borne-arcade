﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTableau : MonoBehaviour
{
    public Vector2 spherecast;
    private Transform _transform;
    public float timeNextFire;
    public bool nextFire = false;
    public GameObject tableBullet1, tableBullet2, tableBullet3, tableBullet4, tableBullet5;

    private Animator _animator;
    // Start is called before the first frame update
    void Start()
    {
        _transform = GetComponent<Transform>();
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _animator.SetFloat("RedEye",0f);//animation eye normal sur les tableaux
        RaycastHit2D[] raycastDetection = Physics2D.CircleCastAll(_transform.position, 5f, transform.position);
        foreach (var hit in raycastDetection)
        {
            if (hit.collider.CompareTag("Player"))// si joueur rentre dans les limite du circle caste alors
            {
                _animator.SetFloat("RedEye",1f);//passage red eye sur les tableaux pour préve nir joueur de la menace
            }
        }
        RaycastHit2D[] raycastAttack = Physics2D.CircleCastAll(_transform.position, 3f, transform.position);
        foreach (var hit in raycastAttack)
        {
            if (hit.collider.CompareTag("Player"))// si joueur rentre dans les limite du circle caste alors
            {
                if (nextFire == false)//tous les if vont permettre de crée une cadence de tiré pour le slime et instantiate le missile
                {
                    
                    FireEnemy();
                    nextFire = true;
                }
                if (nextFire == true)
                {
                    timeNextFire += Time.deltaTime;
                }

                if (timeNextFire >= 1.6f)
                {
                    nextFire = false;
                    timeNextFire = 0f;
                }
                
            }
        }
    }

    void FireEnemy()
    {
        Instantiate(tableBullet1, _transform.position, _transform.rotation);
        Instantiate(tableBullet2, _transform.position, _transform.rotation);
        Instantiate(tableBullet3, _transform.position, _transform.rotation);
        Instantiate(tableBullet4, _transform.position, _transform.rotation);
        Instantiate(tableBullet5, _transform.position, _transform.rotation);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonMenu : MonoBehaviour
{
    public static bool GameIsPaused = false;

    public GameObject pauseMenuUI1;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        
            if (GameIsPaused)
            {
                Resume();
            } else
            {
                Pause();
            }
    }

    public void Resume()
    {
        pauseMenuUI1.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;
    }

    void Pause()
    {
        pauseMenuUI1.SetActive(true);
        Time.timeScale = 0f;
        GameIsPaused = true;
    }

    public void Start()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            SceneManager.LoadScene(3);
            Time.timeScale = 1f;
        }
    }

    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene(0);
    }

    public void QuitGame()
    {
        Debug.Log("Quitting game...");
        Application.Quit();
    }
    public void reStart()
    {
        SceneManager.LoadScene(3);
        Time.timeScale = 1f;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LifeCounter : MonoBehaviour
{
    public TMP_Text lifeText;
    public int life;
    
    void Update()
    {
        life = GameObject.FindWithTag("Player").GetComponent<PlayerLife>().actualLife;
        lifeText.text = ("life :" + life);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class NameEnter : MonoBehaviour
{
    public TextAsset dictionaryTextFile;
    private string allChar;
    public List<string> eachChar;

    public TextMeshProUGUI letter1;
    public TextMeshProUGUI letter2;
    public TextMeshProUGUI letter3;
    
    public TextMeshProUGUI[] letterArray = new TextMeshProUGUI[3];
    public int currentSlot = 0;

    public int currentLetter1 = 0;
    public int currentLetter2 = 0;
    public int currentLetter3 = 0;
    
    public string name;
    
    void Start()
    {
        allChar = dictionaryTextFile.text;
        eachChar = new List<string>();
        eachChar.AddRange(allChar.Split("|"[0]) );

        letterArray[0] = letter1;
        letterArray[1] = letter2;
        letterArray[2] = letter3;
    }

    
    void Update()
    {
        // Gestion de l'emplacement du slot
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            letterArray[currentSlot].color = Color.white;
            currentSlot++;
        }
        else if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            letterArray[currentSlot].color = Color.white;
            currentSlot--;
        }

        if (currentSlot > 2)
        {
            currentSlot = 2;
        }
        else if (currentSlot < 0)
        {
            currentSlot = 0;
        }
        letterArray[currentSlot].color = Color.red;

        
        
        //Gestion du choix du caractère
        if (Input.GetKeyDown((KeyCode.UpArrow)))
        {
            if (currentSlot == 0)
            {
                currentLetter1--;
            }
            else if (currentSlot == 1)
            {
                currentLetter2--;
            }
            else if (currentSlot == 2)
            {
                currentLetter3--;
            }
        }
        else  if (Input.GetKeyDown((KeyCode.DownArrow)))
        {
            if (currentSlot == 0)
            {
                currentLetter1++;
            }
            else if (currentSlot == 1)
            {
                currentLetter2++;
            }
            else if (currentSlot == 2)
            {
                currentLetter3++;
            }
        }

        if (currentLetter1 < 0)
        {
            currentLetter1 = eachChar.Count-1;
        }
        else if (currentLetter1 > eachChar.Count-1)
        {
            currentLetter1 = 0;
        }
        if (currentLetter2 < 0)
        {
            currentLetter2 = eachChar.Count-1;
        }
        else if (currentLetter2 > eachChar.Count-1)
        {
            currentLetter2 = 0;
        }
        if (currentLetter3 < 0)
        {
            currentLetter3 = eachChar.Count-1;
        }
        else if (currentLetter3 > eachChar.Count-1)
        {
            currentLetter3 = 0;
        }
        
        
        
        //Gestion du caractère de chaque slot 
        letterArray[0].text = eachChar[currentLetter1];
        letterArray[1].text = eachChar[currentLetter2];
        letterArray[2].text = eachChar[currentLetter3];

        if (Input.GetButtonDown("Submit"))
        {
            Submit();
        }
    }


    void Submit()
    {
        name = letterArray[0].text + letterArray[1].text + letterArray[2].text;
        Debug.Log(name);
    }
}

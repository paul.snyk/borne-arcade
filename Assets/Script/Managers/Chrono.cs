﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class Chrono : MonoBehaviour
{
    public TMP_Text textChrono;
    public TMP_Text textBestChrono;
    public TMP_Text textTotalChrono;

    public double[] b;
    public float[] bestChrono;
    public float[] numberChrono;
    public static float totalChrono;

    public bool keyInfo;
    // Start is called before the first frame update
    void Start()
    {
        textTotalChrono.text = "Total Time: " + totalChrono;
        if (SceneManager.GetActiveScene().name == "Niveau1")
        {
             bestChrono[0] = PlayerPrefs.GetFloat("Niveau1");
            if (bestChrono[0] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[0] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[0];
            }
        }
        if (SceneManager.GetActiveScene().name == "Niveau2")
        {
            bestChrono[1] = PlayerPrefs.GetFloat("Niveau2");
            if (bestChrono[1] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[1] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[1];
            }
        }
        if (SceneManager.GetActiveScene().name == "Niveau3")
        {
            bestChrono[2] = PlayerPrefs.GetFloat("Niveau3");
            if (bestChrono[2] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[2] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[2];
            }
        }
        if (SceneManager.GetActiveScene().name == "Niveau4")
        {
            bestChrono[3] = PlayerPrefs.GetFloat("Niveau4");
            if (bestChrono[3] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[3] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[3];
            }
        }
        if (SceneManager.GetActiveScene().name == "Niveau5")
        {
            bestChrono[4] = PlayerPrefs.GetFloat("Niveau5");
            if (bestChrono[4] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[4] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[4];
            }
        }
        if (SceneManager.GetActiveScene().name == "Niveau6")
        {
            bestChrono[5] = PlayerPrefs.GetFloat("Niveau6");
            if (bestChrono[5] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[5] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[5];
            }
        }
        if (SceneManager.GetActiveScene().name == "Niveau7")
        {
            bestChrono[6] = PlayerPrefs.GetFloat("Niveau7");
            if (bestChrono[6] == 0f)
            {
                textBestChrono.text = "Best Time : Nothing";
            }
            else if (bestChrono[6] > 0f)
            {
                textBestChrono.text = "Best Time : " + bestChrono[6];
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (SceneManager.GetActiveScene().name == "Niveau1")
        {
            numberChrono[0] += Time.deltaTime;
            b[0] = System.Math.Round(numberChrono[0], 2);
            textChrono.text = "" + b[0];
        }
        if (SceneManager.GetActiveScene().name == "Niveau2")
        {
            numberChrono[1] += Time.deltaTime;
            b[1] = System.Math.Round(numberChrono[1], 2);
            textChrono.text = "" + b[1];
        }
        if (SceneManager.GetActiveScene().name == "Niveau3")
        {
            numberChrono[2] += Time.deltaTime;
            b[2] = System.Math.Round(numberChrono[2], 2);
            textChrono.text = "" + b[2];
        }
        if (SceneManager.GetActiveScene().name == "Niveau4")
        {
            numberChrono[3] += Time.deltaTime;
            b[3] = System.Math.Round(numberChrono[3], 2);
            textChrono.text = "" + b[3];
        }
        if (SceneManager.GetActiveScene().name == "Niveau5")
        {
            numberChrono[4] += Time.deltaTime;
            b[4] = System.Math.Round(numberChrono[4], 2);
            textChrono.text = "" + b[4];
        }
        if (SceneManager.GetActiveScene().name == "Niveau6")
        {
            numberChrono[5] += Time.deltaTime;
            b[5] = System.Math.Round(numberChrono[5], 2);
            textChrono.text = ""+b[5];
        }
        if (SceneManager.GetActiveScene().name == "Niveau7")
        {
            numberChrono[6] += Time.deltaTime;
            b[6] = System.Math.Round(numberChrono[6], 2);
            textChrono.text = "" + b[6];
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Door"))
        {
            if (keyInfo == true)
            {
                if (SceneManager.GetActiveScene().name == "Niveau1")
                {
                    totalChrono = totalChrono + numberChrono[0];
                    if (bestChrono[0] == 0)
                    {
                        bestChrono[0] = numberChrono[0];
                        PlayerPrefs.SetFloat("Niveau1",bestChrono[0]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[0] > numberChrono[0])
                    {
                        bestChrono[0] = numberChrono[0];
                        PlayerPrefs.SetFloat("Niveau1",bestChrono[0]);
                    }
                }
                if (SceneManager.GetActiveScene().name == "Niveau2")
                {
                    totalChrono = totalChrono + numberChrono[1];
                    if (bestChrono[1] == 0)
                    {
                        bestChrono[1] = numberChrono[1];
                        PlayerPrefs.SetFloat("Niveau2",bestChrono[1]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[1] > numberChrono[1])
                    {
                        bestChrono[1] = numberChrono[1];
                        PlayerPrefs.SetFloat("Niveau2",bestChrono[1]);
                    }
                }
                if (SceneManager.GetActiveScene().name == "Niveau3")
                {
                    totalChrono = totalChrono + numberChrono[2];
                    if (bestChrono[2] == 0)
                    {
                        bestChrono[2] = numberChrono[2];
                        PlayerPrefs.SetFloat("Niveau3",bestChrono[2]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[2] > numberChrono[2])
                    {
                        bestChrono[2] = numberChrono[2];
                        PlayerPrefs.SetFloat("Niveau3",bestChrono[2]);
                    }
                }
                if (SceneManager.GetActiveScene().name == "Niveau4")
                {
                    totalChrono = totalChrono + numberChrono[3];
                    if (bestChrono[3] == 0)
                    {
                        bestChrono[3] = numberChrono[3];
                        PlayerPrefs.SetFloat("Niveau4",bestChrono[3]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[3] > numberChrono[3])
                    {
                        bestChrono[3] = numberChrono[3];
                        PlayerPrefs.SetFloat("Niveau4",bestChrono[3]);
                    }
                }
                if (SceneManager.GetActiveScene().name == "Niveau5")
                {
                    totalChrono = totalChrono + numberChrono[4];
                    if (bestChrono[4] == 0)
                    {
                        bestChrono[4] = numberChrono[4];
                        PlayerPrefs.SetFloat("Niveau5",bestChrono[4]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[4] > numberChrono[4])
                    {
                        bestChrono[4] = numberChrono[4];
                        PlayerPrefs.SetFloat("Niveau5",bestChrono[4]);
                    }
                }
                if (SceneManager.GetActiveScene().name == "Niveau6")
                {
                    totalChrono = totalChrono + numberChrono[5];
                    if (bestChrono[5] == 0)
                    {
                        bestChrono[5] = numberChrono[5];
                        PlayerPrefs.SetFloat("Niveau6",bestChrono[5]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[5] > numberChrono[5])
                    {
                        bestChrono[5] = numberChrono[5];
                        PlayerPrefs.SetFloat("Niveau6",bestChrono[5]);
                    }
                }
                if (SceneManager.GetActiveScene().name == "Niveau7")
                {
                    totalChrono = totalChrono + numberChrono[6];
                    if (bestChrono[6] == 0)
                    {
                        bestChrono[6] = numberChrono[6];
                        PlayerPrefs.SetFloat("Niveau7",bestChrono[6]);
                        Debug.Log("rdsgsdgsdgvsedgv");
                    }
                    else if (bestChrono[6] > numberChrono[6])
                    {
                        bestChrono[6] = numberChrono[6];
                        PlayerPrefs.SetFloat("Niveau7",bestChrono[6]);
                    }
                }
                
            }
        }
    }
}

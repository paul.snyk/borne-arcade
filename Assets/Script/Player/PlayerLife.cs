﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerLife : MonoBehaviour
{

    public Transform spawn;

    public static int life = 50;
    public int actualLife;
    
    
    
    void Start()
    {
        
        spawn = GameObject.FindWithTag("Respawn").transform;
        transform.position = spawn.position;
    }

    private void Update()
    {
        actualLife = life;
    }

    public void Damage()
    {
        life--;
        Loader.Reload();
    }
    
}

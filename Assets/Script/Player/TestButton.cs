﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TestButton : MonoBehaviour
{
    public GameObject player;
    
    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    public void TakeDamage()
    {
        player.GetComponent<PlayerLife>().Damage();
    }
}

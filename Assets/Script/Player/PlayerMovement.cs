﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerMovement : MonoBehaviour
{
    private Collision coll;
    [HideInInspector]
    public Rigidbody2D rb;

    [Space]
    [Header("Stats")]
    public float speed = 10;
    public float jumpForce = 50;
    public float slideSpeed = 5;
    public float wallJumpLerp = 10;
    public float dashSpeed = 20;
    public float dashLength = 0.05f;
    public float gravityScale = 3f;
    
    [Space]
    [Header("Booleans")]
    public bool canMove;
    public bool wallSlide;
    public bool wallJumped;
    public bool isDashing;
    public bool lookRight;


    [Space]

    private bool groundTouch;
    private bool hasDashed;
    
    
    public int side = 1;
    
    void Start()
    {
        coll = GetComponent<Collision>();
        rb = GetComponent<Rigidbody2D>();
        canMove = true;
        rb.gravityScale = gravityScale;

    }

    
    void Update()
    {
        //Attribution des inputs
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");
        float xRaw = Input.GetAxisRaw("Horizontal");
        float yRaw = Input.GetAxisRaw("Vertical");
        Vector2 dir = new Vector2(x, y);
        
        //Gestion du déplacement horizontal
        Walk(dir);
        
        //Gestion du sens du player
        if(x > 0)
        {
            side = 1;
            lookRight = true;
        }
        if (x < 0)
        {
            side = -1;
            lookRight = false;
        }

        GetComponent<SpriteRenderer>().flipX = !lookRight;


            //Gestion du saut
        if (Input.GetButtonDown("Jump"))
        {
            if (coll.onGround)
            {
                Jump(Vector2.up); 
            }

            if (wallSlide)
            {
                wallJumped = true;
                WallJump();
            }

        }

        if (coll.onGround)
        {
            wallJumped = false;
        }
        
        
        //Gestion du dash
        if (Input.GetButtonDown("Fire1") && !hasDashed)
        {
            isDashing = true;
            Dash(x,y);
            hasDashed = true;
        }
        if (isDashing && coll.onWall)
        {
            rb.velocity = Vector2.zero;
        }

        if (coll.onGround || coll.onWall)
        {
            hasDashed = false;
        }
        
        
        
        
        //Gestion du wall slide
        if(coll.onWall && !coll.onGround)
        {
            if (x != 0)
            {
                wallSlide = true; 
                WallSlide();
            }
        }
        if (!coll.onWall || coll.onGround)
        {
            wallSlide = false;
        }
        
        
        //Activation de BetterJumping au sol si on dash pas
        if (coll.onGround && !isDashing)
        {
            GetComponent<BetterJumping>().enabled = true;
        }
        
        
        //Gestion du contact au sol et murs
        if (coll.onGround && !groundTouch)
        {
            groundTouch = true;
        }
        else if(!coll.onGround && groundTouch)
        {
            groundTouch = false;
        }

        if (groundTouch && (rb.velocity.x > 0 && coll.onRightWall) || (rb.velocity.x < 0 && coll.onLeftWall))
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }
       
    }
    
    //Fonction du déplacement horizontal
    private void Walk(Vector2 dir)
    {
        if(canMove)
        { 
            if (wallJumped)
            {
                rb.velocity = Vector2.Lerp(rb.velocity, (new Vector2(dir.x * speed, rb.velocity.y)), wallJumpLerp * Time.deltaTime);
            }
            else
            {
                rb.velocity = (new Vector2(dir.x*speed,rb.velocity.y));
            }
        }

        
    }
    
    
    //Fonction du saut
    private void Jump(Vector2 dir)
    {
        rb.velocity = (new Vector2(rb.velocity.x, 0));
        rb.velocity += dir * jumpForce;
    }
    
    
    //Fonction du WallJump
    private void WallJump()
    {
        if ((side == 1 && coll.onRightWall) || side == -1 && !coll.onRightWall)
         {
             side *= -1;
         }
        
        StopCoroutine(DisableMovement(0));
        StartCoroutine(DisableMovement(0.1f));

        Jump(new Vector2(side, 1));

        
    }
    
    
     // Fonction du WallSlide
     private void WallSlide()
     {
         if (coll.wallSide != side)
         {
             if (!canMove)
             {
                 return; 
             }
         }
         if((rb.velocity.x > 0 && coll.onRightWall) || (rb.velocity.x < 0 && coll.onLeftWall))
         {
             rb.velocity = new Vector2(0, -slideSpeed);
         }
     }

     //Fonction du dash
     private void Dash(float x, float y)
     {
         rb.velocity = Vector2.zero;
         rb.gravityScale = 0;
         rb.velocity += new Vector2(x,y).normalized*dashSpeed;
         StopCoroutine(DisableMovement(0));
         StartCoroutine(DisableMovement(dashLength));
         StopCoroutine(EndDash(0));
         StartCoroutine(EndDash(dashLength));
     }
     
     //Enumerateur qui désactive les mouvement
     IEnumerator DisableMovement(float time)
     {
         canMove = false;
         yield return new WaitForSeconds(time);
         canMove = true;
     }

     
     //Enumerateur qui rétablie la phisique à la fin du dash
     IEnumerator EndDash(float time)
     {
         yield return new WaitForSeconds(time);
         rb.velocity = Vector2.zero;
         rb.gravityScale = gravityScale;
         isDashing = false;
     }
}

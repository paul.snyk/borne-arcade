﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour
{

    [Header("Layers")]
    public LayerMask groundLayer;

    [Space]

    public bool onGround;
    public bool onWall;
    public bool onRightWall;
    public bool onLeftWall;
    public int wallSide;

    [Space]

    [Header("Collision")]

    public float collisionRadius = 0.25f;
    public Vector2 bottomOffset, rightOffset, leftOffset;
    private Color debugCollisionColor = Color.red;
    public float collisionLength;
    public float collisionHeight;
    
    
    void Update()
    {  
        onGround = Physics2D.OverlapCircle((Vector2)transform.position + bottomOffset, collisionRadius, groundLayer);
        
        onWall = Physics2D.OverlapBox((Vector2) transform.position + rightOffset,
            new Vector2(collisionLength, collisionHeight),0, groundLayer)
            || Physics2D.OverlapBox((Vector2) transform.position + leftOffset,
            new Vector2(collisionLength, collisionHeight),0, groundLayer);
       
        onRightWall = Physics2D.OverlapBox((Vector2) transform.position + rightOffset,
            new Vector2(collisionLength, collisionHeight),0, groundLayer);
        onLeftWall = Physics2D.OverlapBox((Vector2) transform.position + leftOffset,
            new Vector2(collisionLength, collisionHeight),0, groundLayer);

        wallSide = onRightWall ? -1 : 1;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;

        var positions = new Vector2[] { bottomOffset, rightOffset, leftOffset };

        Gizmos.DrawWireSphere((Vector2)transform.position  + bottomOffset, collisionRadius);
        Gizmos.DrawWireCube((Vector2)transform.position + rightOffset, new Vector2(collisionLength,collisionHeight));
        Gizmos.DrawWireCube((Vector2)transform.position + leftOffset, new Vector2(collisionLength,collisionHeight));
    }
}

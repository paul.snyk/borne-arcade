﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCounter : MonoBehaviour
{
    public GameObject counter;
    public float counterTime = 0.1f;
    public float counterCD = 2f;

    public bool canCounter = true;
    
    void Start()
    {
       counter.SetActive(false);
    }

   
    void Update()
    {
        if (Input.GetButtonDown("Fire2") && canCounter)
        {
            counter.SetActive(true);
            canCounter = false;
            StopCoroutine(endCounter(0));
            StartCoroutine(endCounter(counterTime));
            StopCoroutine(CoolDown(0));
            StartCoroutine(CoolDown(counterCD));
        }
    }

    IEnumerator endCounter(float time)
    {
        yield return new WaitForSeconds(time);
        counter.SetActive(false);
    }

    IEnumerator CoolDown(float time)
    {
        yield return new WaitForSeconds(time);
        canCounter = true;
    }
}
